Title: Kresus 0.21
Date: 2024-05-18 16:04
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-21-0

Une nouvelle version de Kresus est disponible : la 0.21.0. Dans cette version, quelques bugs réparés, quelques nouvelles petites fonctionnalités, quelques nouvelles banques.

**Si vous avez un compte à la Banque Populaire, une action est nécessaire de votre part ; rendez-vous dans la section Support de banque ci-dessous.**

## Les nouveautés

- Les rapports et alertes prennent maintenant en compte le libellé personnalisé, s'il est défini.
- Lorsqu'un compte n'existe plus côté banque, une alerte est affichée dans Kresus dans la page d'édition, et la balance de ce compte peut désormais être forcée manuellement. C'est par exemple utile lorsque la balance est mise à zéro côté banque avant que le compte soit supprimé : Kresus n'en a alors pas connaissance (puisqu'il n'y a plus rien à récupérer) ; et l'intervention manuelle est nécessaire pour avoir un solde correct.
- En cas d'échec la récupération des comptes et opérations est tentée de nouveau quelques fois (utile en cas de souci réseau temporaire, ou de souci temporaire du site de la banque).
- Les transactions récurrentes sont désormais triées par leur jour d'arrivée, dans l'écran de présentation des transactions récurrentes.

Pour les utilisateurs de Kresus sur mobile, l'interface a été modernisée : pour ouvrir les détails vous aviez peut-être l'habitude d'appuyer longtemps sur la ligne d'une opération. À partir de cette version le "swipe" est disponible vers la gauche (demande la suppression de l'opération) ou vers la droite (ouvre les détails de l'opération).
Dans les deux cas, pour éviter de mauvaises manipulations, l'action n'est déclenchée que lorsqu'au moins la moitié de l'action est visible (et devient bleue ou rouge).

![Capture d'écran montrant une opération swipée vers la droite](../images/blog/021-swipable-rows.png)


## Support de banques

Le site de la Banque Populaire nécessitant maintenant de sélectionner une région, ce changement est refleté dans le formulaire d'ajout et d'édition de l'accès bancaire. Si vous avez un compte chez eux : votre accès a été désactivé dans Kresus suite à la migration, il est nécessaire d'aller dans les préférences, puis Accès bancaire, puis cliquer sur le bouton d'édition à côté de l'accès Banque Populaire, et enfin de rentrer les nouvelles informations et le mot de passe de connexion afin de rétablir le lien automatique.

Le support pour une nouvelle banque a été rajoutée : le Crédit Commercial de France (CCF).

Le support des Tickets CESU Edenred été déprécié car le code du connecteur n'est plus maintenu dans Kresus. Ce support pourrait être réactivé plus tard, en fonction des contributions de la communauté Woob.

## Les bugs résolus

- L'ordre des transactions dans le graphique des gains/pertes (le troisième onglet) était inversé par rapport aux labels ; il est de nouveau correct.
- Les transactions futures causaient un rendu incorrect du graphique de la balance (voir [#1248](https://framagit.org/kresusapp/kresus/-/issues/1248) pour les détails). Le rendu a été corrigé, et désormais la ligne de balance apparaît en pointillés pour les transactions futures, pour distinguer la balance réelle qui s'affiche avec une ligne continue.

![Capture d'écran montrant le graphique de la balance avec des transactions futures en pointillés](../images/blog/021-future-transactions-balance-charts.png)


## Notre plus grande histoire d'amour, c'est vous

Comme chaque fois, un grand merci aux contributeurices de Kresus !

Merci à celleux qui ont posté sur notre [forum](https://community.kresus.org)
ou avec qui nous avons parlé directement, pour
leurs retours, questions, encouragement, support, etc.

Merci aux mainteneur.se.s du [paquet
YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) et aux gens qui ouvrent
des tickets quand ça ne marche pas sur YunoHost !

Merci aux contributeur.ice.s en code !

Merci aux [donateur.ice.s](https://liberapay.com/Kresus/), qui nous aident non seulement à financer l'infrastructure (site web, nom de domaine), mais aussi qui nous motivent à continuer le projet et à le rendre meilleur qu'il n'est aujourd'hui !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus), ou via [Matrix
(#kresus:delire.party)](https://matrix.to/#/#kresus:delire.party) !