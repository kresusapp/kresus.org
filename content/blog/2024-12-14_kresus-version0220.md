Title: Kresus 0.22
Date: 2024-12-14 16:17
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-22-0

Une nouvelle version de Kresus est disponible : la 0.22.0. Dans cette version, quelques bugs
réparés et quelques nouvelles petites fonctionnalités pour aider à la synchronisation automatique.

## Les nouveautés

### Désactiver la synchronisation automatique de certains comptes

Depuis quelques mois, les banques demandent de plus en plus souvent des authentifications à deux
facteurs, par exemple en vérifiant sa connexion avec un téléphone mobile. Malheureusement, cela
arrive parfois de manière systématique chez certaines banques, et Kresus se plaindra donc à chaque
synchronisation automatique qu'il est nécessaire d'être présent. Cette nouvelle fonctionnalité
permet d'exclure un compte de la synchro automatique, pour éviter d'avoir à faire avec cette erreur
; il sera donc nécessaire de synchroniser manuellement un tel compte.

Pour désactiver la synchro automatique : allez dans les paramètres, « Accès bancaires », puis le
bouton d'édition à côté du nom de la banque, puis décochez « Synchroniser automatiquement ».

### Fusionner tous les doublons d'un coup

Depuis l'écran de détection des doublons, il est possible de fusionner tous les doublons d'un coup
en cliquant sur un bouton, qui vous demandera tout de même une petite confirmation avant
d'effectuer l'opération. Merci à Cyrille Benoit pour cette contribution !

### Opérations récurrentes : affichage de la liste des mois

Vous rappelez-vous ? Kresus est capable de créer automatiquement en début de mois des *opérations
récurrentes*, qui vont toujours tomber à une date donnée. Pratique pour pré-rentrer les divers
loyers, salaires, et dépenses fréquentes.

Il est possible de préciser que ces opérations n'arrivent que certains mois, un seul mois par an,
ou tous les ans. Depuis cette nouvelle version, l'écran « Transactions récurrentes » précise donc à
quels mois s'appliquent une opération récurrente : tous, un certain nombre (avec en détail
lesquels, en survolant le texte), ou un seul (et donc lequel).

### Ignorer les opérations datant de moins de X jours

Certaines banques ont tendance à rentrer des transactions, puis changer leur libellé au bout de quelques jours, ce qui créé des doublons régulièrement et un peu pénibles. Cette contribution de Cyrille Benoit (merci à lui !) permet de n'importer les opérations que si elles sont plus vieilles de quelques jours, pour éviter cet écueil. Attention ! Activer cette option peut créer de la confusion, car le solde total du compte pourrait inclurer des opérations qui ne sont pas importées dans Kresus…

## Améliorations de l'interface

- Dans la liste des opérations, les dates sont désormais affichées de manière relative (« il y a un
  jour » plutôt que « le 13 décembre 2024 »).
- Sur mobile, dans le graphe des catégories, les légendes sont de nouveau affichées correctement,
  et sont désormais affichées dans l'ordre alphabétique.
- L'écran de création manuelle de transaction a été revue : la date est remplie par défaut au jour
  suivant, le champs de montant est pré-sélectionné par défaut, et l'ordre des différents champs a
  été revu.

## Les bugs résolus

- Dans la liste des opérations, beaucoup de petits bugs ont été résolus liés aux modifications en
  masse. Ils devraient désormais se comporter correctement, mais indiquez-le nous si ce n'est pas le
  cas !
- L'ordre alphabétique des catégories est désormais correctement maintenu après la modification
  d'un nom de catégorie.
- Le bug où le solde total d'un compte n'était pas correct après une synchronisation manuelle a été
  résolu.

## Notre plus grande histoire d'amour, c'est vous

Comme chaque fois, un grand merci aux contributeurices de Kresus !

Merci à celleux qui ont posté sur notre [forum](https://community.kresus.org)
ou avec qui nous avons parlé directement, pour
leurs retours, questions, encouragement, support, etc.

Merci aux mainteneur.se.s du [paquet
YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) et aux gens qui ouvrent
des tickets quand ça ne marche pas sur YunoHost !

Merci aux contributeur.ice.s en code !

Merci aux [donateur.ice.s](https://liberapay.com/Kresus/), qui nous aident non seulement à financer l'infrastructure (site web, nom de domaine), mais aussi qui nous motivent à continuer le projet et à le rendre meilleur qu'il n'est aujourd'hui !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus), ou via [Matrix
(#kresus:delire.party)](https://matrix.to/#/#kresus:delire.party) !
