Title: Kresus 0.20
Date: 2023-11-05 11:00
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-20-0

Une nouvelle version de Kresus est disponible : la 0.20.0 ; pas de gros changements structurels au programme mais plusieurs améliorations
qui amélioreront l'utilisation pour certain⋅e⋅s.

## Nouvelles fonctionnalités

### Séparation des comptes manuels et automatiques sur la page d'embarquement

Sur la page d'embarquement (onboarding), la création d'un nouvel accès est divisée en deux parties distinctes : la création d'un accès automatique, qui récupère directement vos opérations depuis votre banque (via woob), et la création d'un accès manuel, où vous saisissez vous-même chacune de vos opérations (ou bien importez des fichiers OFX).

![capture d'écran montrant la séparation des comptes manuels](../images/blog/020-onboarding-split.png)

### Simplification de l'import des comptes OFX

Toujours à l'embarquement, l'import est également divisé en deux écrans, l'import d'une sauvegarde et l'import d'un fichier OFX :

![capture d'écran de l'écran d'import](../images/blog/020-ofx-import-split.png)

Certain⋅e⋅s utilisent les imports OFX pour importer régulièrement leurs opérations (par exemple dans le cas d'accès bancaires non pris en charge par woob). Jusqu'ici chaque nouvel import créait un nouvel accès, mais cette version permet d'importer les comptes OFX sur un accès existant :

![capture d'écran de l'écran d'import OFX](../images/blog/020-ofx-import-select-access.png)

### Fusion manuelle des comptes

Importer plusieurs comptes c'est bien, mais il est parfois voulu de fusionner certains comptes, par exemple si on importe de vieilles données sur un compte existant, pour en améliorer l'historique.

Dans cette nouvelle version la fusion des comptes d'un même accès est permise à l'utilisateur⋅rice depuis l'écran d'édition d'un compte :

![capture d'écran montrant la fusion de deux comptes](../images/blog/020-merge-accounts.png)

Il est à noter que ça peut entraîner la création de doublons si vous fusionnez des comptes qui ont des opérations en commun. Il vous suffira ensuite de fusionner ces doublons depuis l'écran « Doublons ».

### Modification de la balance des comptes d'un accès désactivé

Suite à une clôture d'un compte bancaire, lorsqu'on souhaite garder l'historique d'un compte, il arrive que la balance ne soit pas mise à zéro, puisque la connexion au site bancaire ne peut plus se faire.

Il est désormais possible de modifier la balance d'un compte sur un accès désactivé, pour rétablir le bon montant.

![capture d'écran montrant l'édition de la balance d'un compte](../images/blog/020-edit-account-balance.png)

### Autres

* Sur mobile la saisie de valeur numérique, par exemple lors de la création d'une opération ou une opération récurrente, est facilitée en ne proposant que les caractères numériques (sur navigateurs qui le supportent).
* Les opérations récurrentes sont maintenant accessibles directement depuis le menu latéral.
* Meilleure découvrabilité de la fonction de création de catégories directement depuis le sélecteur de catégories dans la liste d'opérations
* Plusieurs améliorations visuelles sur les graphiques
* Un bouton permet de copier l'iban d'un compte dans le presse-papiers

## Mise à jour des modules bancaires

* Aviva devient Abeilleassurances
* Barclays devient Milleis
* Bolden est déprécié
* Ajout de Gan Prévoyance
* Ajout de Ma french bank
* Ajout de Roblox
* Ajout de Tiime

## Corrections

Lors de la récupération des opérations, si une alerte était définie mais aucune URL n'était configurée pour Apprise, la requête n'aboutissait jamais et l'utilisateur⋅rice n'en était pas averti⋅e (par exemple lors d'une synchronisation manuelle du compte).

## Notes de mise à jour

La license a été migrée de MIT vers AGPLv3 comme expliqué dans le [billet de blog précédent](./mit-vers-agpl.html).

Pour les sysadmins, devops et autres mainteneuses attentives, voici un résumé
des actions à effectuer pour mettre à jour vers cette version de Kresus.

- La version minimale de woob passe à la 3.5.
- L'utilisation de nss n'est désormais plus prise en charge pour woob, chaque module woob utilisant la connexion la plus appropriée directement.
- API: les endpoints contentant "operation" sont renommés en utilisant "transaction".

## Notre plus grande histoire d'amour, c'est vous

Comme chaque fois, un grand merci aux contributeurices de Kresus !

Merci à celleux qui ont posté sur notre [forum](https://community.kresus.org)
ou avec qui nous avons parlé directement, pour
leurs retours, questions, encouragement, support, etc.

Merci aux mainteneur.se.s du [paquet
YunoHost](https://github.com/YunoHost-Apps/kresus_ynh) et aux gens qui ouvrent
des tickets quand ça ne marche pas sur YunoHost !

Merci aux contributeur.ice.s en code !

Merci aux [donateur.ice.s](https://liberapay.com/Kresus/), qui nous aident non seulement à financer l'infrastructure (site web, nom de domaine), mais aussi qui nous motivent à continuer le projet et à le rendre meilleur qu'il n'est aujourd'hui !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou via [Matrix
(#kresus:delire.party)](https://matrix.to/#/#kresus:delire.party) !
