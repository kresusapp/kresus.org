Title: Kresus adopte la licence AGPLv3.0
Date: 2023-10-13 10:08
Lang: fr
Author: L'équipe de Kresus
Slug: mit-vers-agpl

TL;DR: Kresus est désormais distribué sous la [licence AGPL](https://www.gnu.org/licenses/agpl-3.0.fr.html)

Kresus est un *logiciel libre*. Cela signifie qu'il est distribué sous une *licence libre*, qui assure un certain nombre de libertés aux personnes qui l'utilisent, qu'iels soient administrateur·ice·s ou utilisateur·ice·s :

- la liberté d'utiliser le logiciel, ce qui implique notamment sa gratuité,
- la liberté d'étudier le logiciel, c'est-à-dire de voir comment il marche à travers la publication ouverte de son code source (sa *recette de cuisine*),
- la liberté de copier le logiciel, de le dupliquer légalement afin de le redistribuer,
- et enfin la liberté de le modifier, et de redistribuer les versions modifiées.

En outre, la notion de logiciel libre dépasse ces quatre libertés dites *fondamentales*, qui n'octroient que le label *open-source* à un logiciel. Il s'agit ici d'une interprétation plus personnelle, partagée par les contributeurs principaux, mais derrière le logiciel libre s'ajoute également une volonté de participer à des *communs* numériques, des communs qui bénéficieraient à tous·tes, qui essaient au maximum d'être inclusifs, accessibles et bienveillants auprès de leurs utilisateur·rice·s, afin de participer à la mise en place d'un monde plus juste et meilleur (rien que ça !).

Jusqu'à la version 0.19, Kresus était distribué sous la licence MIT, une licence relativement permissive, choisie principalement par défaut de connaissance et désintérêt du sujet par Benjamin, le contributeur historique de Kresus. Cette licence permet notamment de dupliquer le code initial du projet, d'effectuer des améliorations mais de ne les garder que pour soi, sans en faire profiter qui que ce soit. Notamment, cela permet une récupération commerciale du projet, en imaginant par exemple une structure qui pourrait s'approprier Kresus, l'améliorer, sans rien avoir à demander ou justifier auprès des contributeur·rice·s du projet, ni de nécessiter quelque forme de rétribution. Par ailleurs, cette appropriation pourrait s'accompagner d'un détachement de la volonté de participer à un commun numérique, et se rapprocher d'une volonté capitaliste d'engendrer un profit maximal en partant d'un investissement minimal.

Depuis la version 0.20, Kresus a adopté la licence AGPLv3.0.

# Pourquoi ce changement ?

Tout simplement car nous considérons qu'il est fondamentalement injuste de profiter d'un projet, à des fins pécunières en particulier, sans en faire bénéficier la communauté qui a créé et maintenu ce projet. Nous œuvrons à un projet qui participe aux communs numériques, et nous refusons que des entités s'approprient du travail effectué alors "gratuitement" sans qu'il n'y ait une nécessité morale de contribuer les améliorations en contrepartie.

Contrairement à la licence MIT, la licence AGPL requiert que toute modification apportée au projet soit redistribuée à ses utilisateur·rice·s, à partir du moment où le projet est utilisable/accessible depuis un réseau. Pour le cas de Kresus, cela revient à dire dès que le logiciel est utilisé, étant donné que le logiciel nécessite un serveur Web pour son bon fonctionnement.

Ce qui signifie par exemple que si un géant du numérique type GAFAM souhaite proposer (même à titre gratuit) une version modifiée, adaptée, améliorée de Kresus, dans son cloud, alors ce GAFAM devra partager ses modifications en retour, au moins aux gens qui l'utilisent, et donc par extension à toute la communauté d'utilisateur·rice·s de Kresus.

# Comment cela m'impacte-t-il ?

**Si je n'ai apporté aucune modification au code source de Kresus pour l'adapter à mes besoins, rien ne change pour moi.**

## Je n'ai rien changé à Kresus, je suis utilisateur·rice

C'est parfait, vous pouvez continuer comme ça. N'hésitez-pas à passer nous [faire coucou](https://matrix.to/#/#kresus:delire.party) ou nous [dire merci](https://tutut.delire.party/@kresus), à nous ou au [projet Woob](https://matrix.to/#/#woob:matrix.org). On apprécie toujours beaucoup des retours de personnes satisfaites. Et on est aussi là pour les bugs à nous rapporter (mais bien entendu, il n'y a pas de bugs dans Kresus 😇).

## J'ai modifié le code de Kresus

Si j'ai apporté des modifications, que ce soit pour moi ou pour d'autres, il y a des chances qu'elles plaisent à d'autres membres de la communauté, et dans cet esprit la licence demande que les modifications soient publiées auprès des utilisateur·rice·s de la version modifiée. Nous, on voudrait bien que ces modifications nous soient également partagées, afin qu'elles bénéficient à tous·tes, mais vous faites bien comme vous voulez !

## Je commercialise un produit basé sur Kresus

Je bénéficie déjà de tous les développements effectués sur Kresus. Il est juste de continuer à améliorer le projet initial, que ce soit en termes de fonctionnalités, d'améliorations de performances ou de sécurité. Si mon produit me rapporte de l'argent, je peux imaginer "donner en retour", sous la forme d'heures de travail salarié destiné à des contributions au projet, notamment pour re-proposer les modifications effectuées. Il y a d'ailleurs des chances que ces modifications proposées soient intégrées au projet, et donc que le maintien en soit pris en charge par les mainteneur·se·s, ce qui facilitera toutes les montées en version. Encore une fois, en dehors de partager les sources avec vos utilisateur·rice·s, rien ne vous y oblige, mais ce serait chouette de participer à l'aventure du commun numérique.

# Comment partager ses modifications ?

D'après la licence, le strict minimum est donc de permettre le téléchargement des sources depuis le logiciel modifié. Mais pour en faire réellement bénéficier la communauté, le plus intéressant sera d'ouvrir une [merge request sur Framagit](https://framagit.org/kresusapp/kresus/-/merge_requests) ou sur [Github](https://github.com/kresusapp/kresus).

# En conclusion 

Si pour la majorité de la communauté ce changement de licence n'aura pas d'impact direct, nous pensons qu'il sera quand bien même bénéfique à toutes et tous sur le long terme, et permettra au projet une certaine sérénité et durabilité. Merci d'avoir lu jusqu'au bout, et à bientôt !
